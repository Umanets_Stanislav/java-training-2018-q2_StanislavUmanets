<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="menu.jsp"%>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1>
					<c:out value="${user.firstName}" />
					&nbsp;
					<c:out value="${user.lastName}" />
				</h1>
				<p>
					User login:
					<c:out value="${user.login}" />
				</p>
				<h3>My Tests</h3>
				<table class="table table-condensed table-hover">
					<thead>
						<tr>
							<th>№</th>
							<th>Name of test</th>
							<th>Difficulty</th>
							<th>Result</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${resultList}" varStatus="count">
							<tr>
								<td><c:out value="${count.count}" /></td>
								<td><c:out value="${item.nameTest}" /></td>
								<td><c:out value="${item.difficulty}" /></td>
								<td><c:out value="${item.result}" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>