<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Result</title>
</head>
<body>
	<%@ include file="menu.jsp"%>
	<div id="res">
		<div id="info">
			<h3>
				<c:out value="${testName}" />
			</h3>
			<h3>
				Number of questions:
				<c:out value="${test.questionNumber}" />
			</h3>
			<h3>
				Difficulty:
				<c:out value="${test.difficulty}" />
			</h3>
			<h3>
				Your result:
				<c:out value="${result.result}"></c:out>
			</h3>
		</div>
	</div>
</body>
</html>