<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<title>User menu</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<%@ include file="menu.jsp" %>
	<p id="error">
		<c:if test="${not empty error}">
			<c:out value="${error}" />
		</c:if>
	</p>
	<h2 style="margin-left: 20px;">
		Hello
		<c:out value="${user.login}" />
	</h2>
	<form action="select" method="get">
		<div id="select">
			<div class="form-group">
				<label for="sel2">Select subject: </label><select
					class="form-control" name="subject"
					title="Choose one of the following..." id="sel2">
					<option value="math_test">Math</option>
					<option value="english_test">English</option>
					<option value="biology_test">Biology</option>
				</select> <label for="sel1">Select difficulty:</label><select
					class="form-control" name="difficulty"
					title="Choose one of the following..." id="sel1">
					<option value="easy">Easy</option>
					<option value="middle">Middle</option>
					<option value="hard">Hard</option>
				</select><br>
			</div>
		</div>
		<div id="pos">
			<button type="submit" class="btn btn-primary">Start</button>
		</div>
	</form>
</body>
</html>