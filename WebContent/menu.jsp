<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="lib.jspf" %>
<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="user_menu.jsp">Tests</a>
			</div>

			<div id="wrapper">
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="alltests">All tests</a></li>
						<li><a href="mypage" >My page</a></li>
						<li><a href="logout" >Log out</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>