<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE body PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Test</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<%@ include file="menu.jsp"  %>
	<div id="info">
		<h3>
			<c:out value="${testName}" />
		</h3>
		<h3>
			Number of questions:
			<c:out value="${test.questionNumber}" />
		</h3>
		<h3>
			Difficulty:
			<c:out value="${test.difficulty}" />
		</h3>
	</div>
	<div id="list">
		<form action="checktest" method="post">
			<c:forEach var="entry" items="${questions}" varStatus="count">
				<h4>
					<c:out value="${entry.key}" />
				</h4>
				<c:forEach var="item" items="${entry.value}" varStatus="number">
					<div class="form-check">
						<label class="form-check-label"> <input type="radio"
							class="form-check-input" name="${count.count}" value="${number.count}">
							<c:out value="${item}" /></label>
					</div>
				</c:forEach>
			</c:forEach>
			<div class="wide"><button id="search-input" type="submit" class="btn btn-primary">Finish</button></div>
		</form>
	</div>
</body>
</html>