package ua.nure.umanets.SummaryTask.exception;

public class DBException extends Exception {

	private static final long serialVersionUID = 6842239745969840575L;

	public DBException() {
		super();
	}

	public DBException(String message) {
		super(message);
	}

	public DBException(String message, Throwable cause) {
		super(message, cause);
	}
}
