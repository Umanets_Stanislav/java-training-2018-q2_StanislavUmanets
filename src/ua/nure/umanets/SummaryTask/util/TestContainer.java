package ua.nure.umanets.SummaryTask.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import ua.nure.umanets.SummaryTask.model.Test;

public class TestContainer {

	private static String[] fileArray = new String[] { "C:\\Users\\PILOT\\workspace\\SummaryTask\\WebContent\\resources\\biology_test.txt", "C:\\Users\\PILOT\\workspace\\SummaryTask\\WebContent\\resources\\english_test.txt", "C:\\Users\\PILOT\\workspace\\SummaryTask\\WebContent\\resources\\math_test.txt"};

	private static Map<String, Test> tests = new HashMap<>();
	
	static{
		fillContainer();
	}
	
	private TestContainer() {
	}

	private static  void fillContainer() {
		for (int i = 0; i < fileArray.length; i++) {
			Test test = new Test();
			try (FileReader fr = new FileReader(fileArray[i])) {
				try (Scanner scan = new Scanner(fr)) {
					while (scan.hasNextLine()) {
						String line = scan.nextLine();
						if (line.contains("Difficulty")) {
							int index = line.indexOf("=");
							String difficulty = line.substring(index + 1, line.length());
							if (test.getDifficulty() != null) {
								test.setName(parseFileName(fileArray[i], test));
								test.setQuestionNumber();
								tests.put(test.getName(), test);
								test = new Test();
							}
							test.setDifficulty(difficulty);
						} else {
							test.getQuestionList().add(line);
						}
						if (!scan.hasNextLine()) {
							test.setName(parseFileName(fileArray[i], test));
							test.setQuestionNumber();
							tests.put(test.getName(), test);
						}
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static String parseFileName(String name, Test test) {
		String path = Paths.get(name).getFileName().toString();
		String file = path.substring(0, path.length() - 4) + "_" + test.getDifficulty();
		return file;
	}

	public static Test getTest(String subject, String difficulty){
		String testName = subject + "_" + difficulty;
		return tests.get(testName);
	}
}
