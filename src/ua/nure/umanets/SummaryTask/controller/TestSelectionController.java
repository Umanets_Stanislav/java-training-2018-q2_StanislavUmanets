package ua.nure.umanets.SummaryTask.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.umanets.SummaryTask.model.Test;
import ua.nure.umanets.SummaryTask.util.TestContainer;

/**
 * Controller for processing the user select
 * 
 * @author S.Umanets
 */
public class TestSelectionController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TestSelectionController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		Map<String, List<String>> questions;
		
		session.removeAttribute("testName");
		session.removeAttribute("test");
		session.removeAttribute("questions");
		
		String subject = request.getParameter("subject");
		String difficulty = request.getParameter("difficulty");

		Test test = TestContainer.getTest(subject, difficulty);
		
		questions = processTest(test);
		String testName = getNameForTest(test.getName());
		
		session.setAttribute("testName", testName);
		session.setAttribute("test", test);
		session.setAttribute("questions", questions);
		response.sendRedirect("testpage.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private Map<String, List<String>> processTest(Test test) {
		Map<String, List<String>> questions = new LinkedHashMap<>();
		String pattern = "^[0-9]{1,}[\\.]{1}";
		Pattern p = Pattern.compile(pattern);
		List<String> answers = new ArrayList<>();
		String question = null;
		for (int i = 0; i < test.getQuestionList().size(); i++) {
			String str = test.getQuestionList().get(i);

			Matcher m = p.matcher(str);
			if (m.find()) {
				if (question != null) {
					questions.put(question, answers);
					answers = new ArrayList<>();
				}
				question = str;
			} else if (i == test.getQuestionList().size() - 1) {
				answers.add(str);
				questions.put(question, answers);
			} else {
				answers.add(str);
			}
		}
		return questions;
	}

	private String getNameForTest(String name) {
		String[] arr = name.split("_");
		String a = arr[0].substring(0, 1).toUpperCase() + arr[0].substring(1).toLowerCase();
		String b = arr[1].substring(0, 1).toUpperCase() + arr[1].substring(1).toLowerCase();
		StringBuilder sb = new StringBuilder();
		sb.append(a + " ").append(b);
		return sb.toString();
	}
}
