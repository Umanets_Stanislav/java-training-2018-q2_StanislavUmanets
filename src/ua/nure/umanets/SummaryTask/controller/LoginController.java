package ua.nure.umanets.SummaryTask.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.umanets.SummaryTask.dao.UserDao;
import ua.nure.umanets.SummaryTask.dao.impl.UserDaoImpl;
import ua.nure.umanets.SummaryTask.model.User;

public class LoginController extends HttpServlet {

	private UserDao userDao = new UserDaoImpl();

	private static final long serialVersionUID = 1548097400746334307L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final HttpServletRequest req = (HttpServletRequest) request;
		final HttpServletResponse resp = (HttpServletResponse) response;

		final String login = req.getParameter("login");
		final String password = req.getParameter("password");

		final HttpSession session = req.getSession();
		User user;

		if (session != null && session.getAttribute("user")!= null) {
			System.out.println("Session exist. User logged in");
			user = (User) session.getAttribute("user");
			moveToMenu(req, resp, user.getRoleId());
		} else if ((user = userIsExist(login, password)) != null) {
			System.out.println("Add user to session");
			session.setAttribute("user", user);
			int interval = session.getMaxInactiveInterval();
			session.setAttribute("interval", interval);
			moveToMenu(req, resp, user.getRoleId());
		} else {
			System.out.println("User not exist");
			moveToMenu(req, resp, 2);
		}
	}

	private User userIsExist(String login, String password) {
		User user = userDao.findByLogin(login);
		if (user.getLogin() != null) {
			if (user.getPassword().equals(password) && user.getLogin().equals(login)) {
				return user;
			}
		}
		return null;
	}

	private void moveToMenu(HttpServletRequest req, HttpServletResponse resp, int roleId)
			throws ServletException, IOException {
		if (roleId == 1) {
			resp.sendRedirect("user_menu.jsp");
		} else if (roleId == 0) {
			resp.sendRedirect("admin_menu.jsp");
		} else {
			String loginError = "Wrong login or password";
			req.getSession().setAttribute("loginError", loginError);
			resp.sendRedirect("login_register_error.jsp");
		}
	}

}
