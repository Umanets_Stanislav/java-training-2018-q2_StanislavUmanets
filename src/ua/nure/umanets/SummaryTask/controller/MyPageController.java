package ua.nure.umanets.SummaryTask.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.umanets.SummaryTask.dao.TestResultDao;
import ua.nure.umanets.SummaryTask.dao.impl.TestResultDaoImpl;
import ua.nure.umanets.SummaryTask.model.Result;
import ua.nure.umanets.SummaryTask.model.User;

/**
 * @author S.Umanets
 */
public class MyPageController extends HttpServlet {
	private static final long serialVersionUID = -1062216800000676064L;
	
	
	private TestResultDao testDao = new TestResultDaoImpl(); 

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		List<Result> resultList = testDao.listOfResult(user);
		
		session.setAttribute("resultList", resultList);
		response.sendRedirect("mypage.jsp");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
