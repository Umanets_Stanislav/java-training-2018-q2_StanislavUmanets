package ua.nure.umanets.SummaryTask.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.umanets.SummaryTask.dao.UserDao;
import ua.nure.umanets.SummaryTask.dao.impl.UserDaoImpl;
import ua.nure.umanets.SummaryTask.model.User;

/**
 * 
 */
public class RegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UserDao userDao = new UserDaoImpl();

	@SuppressWarnings("unused")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		String firstName = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");

		User user = userDao.findByLogin(login);
		
		if (user.getLogin() == null) {
			user.setLogin(login);
			user.setPassword(password);
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setRoleId(1);
			userDao.addUser(user);
			session.setAttribute("user", user);
			response.sendRedirect("user_menu.jsp");
		} else {
			String regError = "User with login " + login + " already exist!";
			session.setAttribute("regError", regError);
			response.sendRedirect("login_register_error.jsp");
		}

	}

}
