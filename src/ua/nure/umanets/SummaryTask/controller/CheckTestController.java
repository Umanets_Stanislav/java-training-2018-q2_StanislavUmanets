package ua.nure.umanets.SummaryTask.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.umanets.SummaryTask.dao.TestResultDao;
import ua.nure.umanets.SummaryTask.dao.impl.TestResultDaoImpl;
import ua.nure.umanets.SummaryTask.model.Result;
import ua.nure.umanets.SummaryTask.model.Test;
import ua.nure.umanets.SummaryTask.model.User;

/**
 * 
 * 
 * @author S.Umanets
 */
public class CheckTestController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private TestResultDao resultDao = new TestResultDaoImpl();

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Result result = new Result();
		List<Boolean> trueAnswers = new ArrayList<>();
		Enumeration<String> en = request.getParameterNames();
		Random r = new Random();

		while (en.hasMoreElements()) {
			String param = en.nextElement();
			String value = request.getParameter(param);
			int randomNumber = r.nextInt(4) + 1;
			if (randomNumber == Integer.valueOf(value)) {
				trueAnswers.add(true);
			}
		}

		User user = (User) session.getAttribute("user");
		Test test = (Test) session.getAttribute("test");

		double questionNumber = test.getQuestionNumber();
		double rightAnswers = trueAnswers.size();
		double res = (100 * rightAnswers) / questionNumber;
		double newDouble = new BigDecimal(res).setScale(2, RoundingMode.UP).doubleValue();
	
		String percentResult = String.valueOf(newDouble) + "%";
		
		result.setResult(percentResult);
		result.setDifficulty(test.getDifficulty());
		result.setNameTest(getNameForTest(test.getName()));
		
		resultDao.addResult(result, user);

		session.setAttribute("result", result);
		response.sendRedirect("result.jsp");
	}
	
	private String getNameForTest(String name) {
		String[] arr = name.split("_");
		String a = arr[0].substring(0, 1).toUpperCase() + arr[0].substring(1).toLowerCase();
		String b = arr[1].substring(0, 1).toUpperCase() + arr[1].substring(1).toLowerCase();
		StringBuilder sb = new StringBuilder();
		sb.append(a + " ").append(b);
		return sb.toString();
	}
}
