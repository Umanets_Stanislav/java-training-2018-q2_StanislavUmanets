package ua.nure.umanets.SummaryTask.model;

public enum Role {
	USER, ADMIN;
	
	public static Role getRole(User user) {
		int roleId = user.getRoleId();
		return Role.values()[roleId];
	}
	
	public String getName() {
		return name().toLowerCase();
	}
}
