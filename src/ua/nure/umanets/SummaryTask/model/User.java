package ua.nure.umanets.SummaryTask.model;

public class User extends Entity {

	private static final long serialVersionUID = 5431990093676430486L;

	private String login;

	private String password;

	private String firstName;

	private String lastName;

	private int roleId;
	
	
	public User() {
		super();
	}

	public User(String login, String password, String firstName, String lastName, int roleId) {
		super();
		this.login = login;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.roleId = roleId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return "User [login=" + login + ", password=" + password + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", roleId=" + roleId + ", id=" + getId() + "]";
	}
	
	
}
