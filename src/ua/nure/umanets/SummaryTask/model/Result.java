package ua.nure.umanets.SummaryTask.model;

public class Result extends Entity{
	private static final long serialVersionUID = -3360160367819894669L;
	
	private String nameTest;
	private String difficulty;
	private String result;

	public String getNameTest() {
		return nameTest;
	}
	public void setNameTest(String nameTest) {
		this.nameTest = nameTest;
	}
	public String getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	@Override
	public String toString() {
		return "Result [nameTest=" + nameTest + ", difficulty=" + difficulty + ", result=" + result + "]";
	}
}
