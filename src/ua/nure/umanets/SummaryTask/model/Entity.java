package ua.nure.umanets.SummaryTask.model;

import java.io.Serializable;

public abstract class Entity implements Serializable {
	
	private static final long serialVersionUID = 5685918444366983768L;
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
