package ua.nure.umanets.SummaryTask.model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {

	private String name;
	private String difficulty;
	private int questionNumber;
	private List<String> questionList = new ArrayList<>();;

	public Test() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public int getQuestionNumber() {
		return questionNumber;
	}

	public void setQuestionNumber() {
		String pattern = "^[0-9]{1,}[\\.]{1}";
		Pattern p = Pattern.compile(pattern);
		int count = 0;
		for (int i = 0; i < questionList.size(); i++) {
			Matcher m = p.matcher(questionList.get(i));
			if (m.find()) {
				count++;
			}
		}
		questionNumber = count;
	}

	public List<String> getQuestionList() {
		return questionList;
	}

	@Override
	public String toString() {
		return "Test [name=" + name + ", difficulty=" + difficulty + ", questionNumber=" + questionNumber
				+ ", questionList=" + questionList + "]";
	}
	
	
	
}
