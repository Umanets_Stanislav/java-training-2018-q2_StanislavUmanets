package ua.nure.umanets.SummaryTask.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.umanets.SummaryTask.dao.UserDao;
import ua.nure.umanets.SummaryTask.dao.impl.UserDaoImpl;
import ua.nure.umanets.SummaryTask.model.User;

/**
 * Check session filter checks if the session exists
 * 
 * @author S.Umanets
 */
public class CheckSessionFilter implements Filter {

	public CheckSessionFilter() {

	}

	public void destroy() {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final HttpServletRequest req = (HttpServletRequest) request;
		final HttpServletResponse resp = (HttpServletResponse) response;

		final HttpSession session = req.getSession(false);

		if (session != null) {
			System.out.println("Session not null");

		} else {
			System.out.println("Session not exist/forward to login.html");
			resp.sendRedirect("login.html");
			return;
		}
		System.out.println("end of check session");
		chain.doFilter(req, resp);
	}

	public void init(FilterConfig fConfig) throws ServletException {

	}

}
