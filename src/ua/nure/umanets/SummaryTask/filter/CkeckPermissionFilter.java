package ua.nure.umanets.SummaryTask.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.umanets.SummaryTask.model.User;

/**
 * Ckeck Permission Filter
 * 
 * @author S.Umanets
 */
public class CkeckPermissionFilter implements Filter {

	public CkeckPermissionFilter() {

	}

	public void destroy() {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final HttpServletRequest req = (HttpServletRequest) request;
		final HttpServletResponse resp = (HttpServletResponse) response;

		String adminPage = "/admin_menu.jsp";
		String uriPath = req.getRequestURI().substring(req.getContextPath().length());
		System.out.println(uriPath);

		HttpSession session = req.getSession(false);

		if (uriPath.equals(adminPage)) {
			User user = (User) session.getAttribute("user");

			if (user.getRoleId() == 0) {
				System.out.println("This is admin!");
			} else {
				System.out.println("This is user!");
				String error = "You don't have permission for that page!";
				session.setAttribute("error", error);
				resp.sendRedirect("user_menu.jsp");
			}
		}
		System.out.println("Request doesn't contain admin page.");
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {

	}

}
