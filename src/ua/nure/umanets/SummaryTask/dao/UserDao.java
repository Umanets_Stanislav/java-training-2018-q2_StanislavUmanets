package ua.nure.umanets.SummaryTask.dao;

import ua.nure.umanets.SummaryTask.model.User;

public interface UserDao {
	
	void addUser(User user);
	
	User findUserById(int id);
	
	User findByLogin(String login);
	
	void updateUser(User user);
	
	void deleteUser(User user);
	
}
