package ua.nure.umanets.SummaryTask.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ua.nure.umanets.SummaryTask.dao.TestResultDao;
import ua.nure.umanets.SummaryTask.db.DBManager;
import ua.nure.umanets.SummaryTask.exception.DBException;
import ua.nure.umanets.SummaryTask.model.Result;
import ua.nure.umanets.SummaryTask.model.User;

/**
 * Dao class for {@link Result}
 * 
 * @author S.Umanets
 */
public class TestResultDaoImpl implements TestResultDao {

	private DBManager manager = DBManager.getInstance();

	@Override
	public void addResult(Result result, User user) {
		String sql = "INSERT INTO results (name_test, difficulty, result, user_id) VALUES(?,?,?,?)";

		try (Connection con = manager.getConnection()) {
			try (PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
				ps.setString(1, result.getNameTest());
				ps.setString(2, result.getDifficulty());
				ps.setString(3, result.getResult());
				ps.setInt(4, user.getId());
				int changedRow = ps.executeUpdate();
				if (changedRow > 0) {
					try (ResultSet rs = ps.getGeneratedKeys()) {
						while (rs.next()) {
							result.setId(rs.getInt(1));
						}
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DBException e) {
			e.printStackTrace();
		}
		System.out.println("Resul added!");
	}

	@Override
	public void cleanResults(User user) {
		String sql = "DELETE FROM result WHERE user_id=?";
		try (Connection con = manager.getConnection()) {
			try (PreparedStatement ps = con.prepareStatement(sql)) {
				ps.setInt(1, user.getId());
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DBException e) {
			e.printStackTrace();
		}
		System.out.println("Resul cleaned!");
	}

	@Override
	public Result getResult(User user) {
		String sql = "SELEC * FROM results WHERE user_id=?";
		Result result = new Result();
		try (Connection con = manager.getConnection()) {
			try (PreparedStatement ps = con.prepareStatement(sql)) {
				ps.setInt(1, user.getId());
				try (ResultSet rs = ps.executeQuery()) {
					while (rs.next()) {
						result.setId(rs.getInt("id"));
						result.setNameTest(rs.getString("name_test"));
						result.setDifficulty(rs.getString("difficulty"));
						result.setResult(rs.getString("result"));
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DBException e) {
			e.printStackTrace();
		}
		System.out.println("Resul found!");
		return result;
	}

	@Override
	public List<Result> listOfResult(User user) {
		List<Result> resultList = new ArrayList<>();

		String sql = "SELECT * FROM results WHERE user_id=?";

		try (Connection con = manager.getConnection()) {
			try (PreparedStatement ps = con.prepareStatement(sql)) {
				ps.setInt(1, user.getId());
				try (ResultSet rs = ps.executeQuery()) {
					while (rs.next()) {
						Result result = new Result();
						result.setId(rs.getInt("id"));
						result.setNameTest(rs.getString("name_test"));
						result.setDifficulty(rs.getString("difficulty"));
						result.setResult(rs.getString("result"));
						resultList.add(result);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DBException e) {
			e.printStackTrace();
		}
		System.out.println(resultList);
		return resultList;
	}
}
