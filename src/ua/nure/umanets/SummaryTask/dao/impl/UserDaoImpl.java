package ua.nure.umanets.SummaryTask.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ua.nure.umanets.SummaryTask.dao.UserDao;
import ua.nure.umanets.SummaryTask.db.DBManager;
import ua.nure.umanets.SummaryTask.exception.DBException;
import ua.nure.umanets.SummaryTask.model.Result;
import ua.nure.umanets.SummaryTask.model.User;

/**
 * Dao class for {@link User}
 * 
 * @author S.Umanets
 */
public class UserDaoImpl implements UserDao {

	private DBManager manager = DBManager.getInstance();

	@Override
	public void addUser(User user) {
		String sql = "INSERT INTO users (login, password, first_name, last_name, role_id) VALUES(?,?,?,?,?)";

		try (Connection con = manager.getConnection()) {
			try (PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
				ps.setString(1, user.getLogin());
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getFirstName());
				ps.setString(4, user.getLastName());
				ps.setInt(5, user.getRoleId());
				int changedRow = ps.executeUpdate();
				if (changedRow > 0) {
					try (ResultSet rs = ps.getGeneratedKeys()) {
						if (rs.next()) {
							user.setId(rs.getInt(1));
						}
					}
				}
			}
		} catch (SQLException e) {

			e.printStackTrace();
		} catch (DBException e) {
			e.printStackTrace();
		}
		System.out.println("User successfully added");
	}

	@Override
	public User findUserById(int id) {
		String sql = "SELECT * FROM users WHERE id = ?";
		User user = new User();

		try (Connection con = manager.getConnection()) {
			try (PreparedStatement ps = con.prepareStatement(sql)) {
				ps.setInt(1, id);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						user.setId(id);
						user.setLogin(rs.getString("login"));
						user.setPassword(rs.getString("password"));
						user.setFirstName(rs.getString("first_name"));
						user.setLastName(rs.getString("last_name"));
						user.setRoleId(rs.getInt("role_id"));
					}
				}
			}

		} catch (SQLException e) {

			e.printStackTrace();
		} catch (DBException e) {

			e.printStackTrace();
		}
		System.out.println("User found!");
		return user;
	}

	@Override
	public User findByLogin(String login) {
		String sql = "SELECT * FROM users WHERE login = ?";
		User user = new User();

		try (Connection con = manager.getConnection()) {
			try (PreparedStatement ps = con.prepareStatement(sql)) {
				ps.setString(1, login);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						user.setId(rs.getInt("id"));
						user.setLogin(login);
						user.setPassword(rs.getString("password"));
						user.setFirstName(rs.getString("first_name"));
						user.setLastName(rs.getString("last_name"));
						user.setRoleId(rs.getInt("role_id"));
					}
				}
			}
		} catch (SQLException e) {

			e.printStackTrace();
		} catch (DBException e) {

			e.printStackTrace();
		}
		System.out.println("User found!");
		return user;
	}

	@Override
	public void updateUser(User user) {

		String sql = "UPDATE users SET login=?, password=?, first_name=?, last_name=?, role_id=? WHERE id=?";

		try (Connection con = manager.getConnection()) {
			try (PreparedStatement ps = con.prepareStatement(sql)) {
				ps.setString(1, user.getLogin());
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getFirstName());
				ps.setString(4, user.getLastName());
				ps.setInt(5, user.getRoleId());
				ps.setInt(6, user.getId());
				ps.executeUpdate();
			}
		} catch (SQLException e) {

			e.printStackTrace();
		} catch (DBException e) {

			e.printStackTrace();
		}
		System.out.println("User updated!");
	}

	@Override
	public void deleteUser(User user) {
		String sql = "DELETE FROM users WHERE login=?";

		try (Connection con = manager.getConnection()) {
			try (PreparedStatement ps = con.prepareStatement(sql)) {
				ps.setString(1, user.getLogin());
				ps.executeUpdate();
			}
		} catch (SQLException e) {

			e.printStackTrace();
		} catch (DBException e) {

			e.printStackTrace();
		}
		System.out.println("User deleted!");
	}

}
