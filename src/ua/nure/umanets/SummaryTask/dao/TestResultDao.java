package ua.nure.umanets.SummaryTask.dao;

import java.util.List;

import ua.nure.umanets.SummaryTask.model.Result;
import ua.nure.umanets.SummaryTask.model.User;

public interface TestResultDao {
	
	void addResult(Result result, User user);
	
	void cleanResults(User user);
	
	Result getResult(User user);
	
	List<Result> listOfResult(User user);
}
