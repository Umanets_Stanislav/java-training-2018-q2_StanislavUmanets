DROP TABLE users;
DROP TABLE roles;
DROP TABLE results;

-----------------------
--table users
-----------------------
CREATE TABLE IF NOT EXISTS users (
	id  serial PRIMARY KEY,
	login VARCHAR(20) NOT NULL UNIQUE,
	password VARCHAR(20) NOT NULL, 
	first_name VARCHAR(20) NOT NULL,
	last_name VARCHAR(20) NOT NULL,
	role_id INTEGER NOT NULL REFERENCES roles(id) ON DELETE CASCADE ON UPDATE RESTRICT
);

------------------------------
--insert user into table users
------------------------------
INSERT INTO users (login, password, first_name, last_name, role_id) VALUES ('iv', 'password', 'ivan', 'ivanov', '1');


-----------------------
--table roles
-----------------------
CREATE TABLE IF NOT EXISTS roles (
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(10) NOT NULL 
); 


------------------------------
--insert user into table roles
------------------------------
INSERT INTO roles (id, name) VALUES('0', 'admin');
INSERT INTO roles (id, name) VALUES('1', 'user');

-----------------------
--table results
-----------------------
CREATE TABLE IF NOT EXISTS results (
	id serial PRIMARY KEY,
	name_test VARCHAR(25) NOT NULL,
	difficulty VARCHAR(15) NOT NULL,
	result VARCHAR(10) NOT NULL,
	user_id  INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE ON UPDATE RESTRICT
); 

